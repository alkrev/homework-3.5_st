# homework-3.5. Настройка нагрузочного тестирования

Для демонстрации нагрузочного тестирования использована база данных:
https://edu.postgrespro.ru/demo-big.zip

### Запуск сервиса:
go run ./cmd/stress/main.go
### Команды нагрузочного тестирования:
- ghz --insecure --proto api.proto --call api.AirCraftFlightsService.SelectAirCraftFlights -d '{"model": "Боинг 777-300"}' localhost:8080
- ghz --insecure --proto api.proto --call api.AirCraftFlightsService.SelectAirCraftFlights2 -d '{"model": "Боинг 777-300"}' localhost:8080

### Нагрузочное тестирования первой ручки:

Запрос: 

        select
            f.flight_id,
            f.flight_no,
            f.scheduled_departure,
            f.scheduled_arrival,
            f.departure_airport,
            f.arrival_airport,
            f.status,
            f.aircraft_code,
            f.actual_departure,
            f.actual_arrival
        from aircrafts_data a, flights f, aircrafts ar
        where a.aircraft_code = (SELECT aircraft_code FROM aircrafts WHERE model = $1) and a.aircraft_code = f.aircraft_code;

Результаты:

    Summary:
    Count:        200
    Total:        46.64 s
    Slowest:      12.29 s
    Fastest:      1.09 s
    Average:      10.35 s
    Requests/sec: 4.29
    
    Response time histogram:
    1089.606  [1]   |
    2209.307  [7]   |∎∎
    3329.008  [4]   |∎
    4448.709  [4]   |∎
    5568.410  [6]   |∎∎
    6688.111  [6]   |∎∎
    7807.812  [4]   |∎
    8927.513  [4]   |∎
    10047.214 [6]   |∎∎
    11166.915 [10]  |∎∎∎
    12286.616 [148] |∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎
    
    Latency distribution:
    10 % in 4.89 s
    25 % in 11.10 s
    50 % in 11.57 s
    75 % in 11.86 s
    90 % in 12.05 s
    95 % in 12.13 s
    99 % in 12.23 s
    
    Status code distribution:
    [OK]   200 responses


### Нагрузочное тестирования второй ручки:

Запрос:

        select 
          f.flight_id,
          f.flight_no,
          f.scheduled_departure,
          f.scheduled_arrival,
          f.departure_airport,
          f.arrival_airport,
          f.status,
          f.aircraft_code,
          f.actual_departure,
          f.actual_arrival
        from aircrafts_data a, flights f, aircrafts ar 
        where a.aircraft_code = ar.aircraft_code and ar.aircraft_code = f.aircraft_code and ar.model = $1;

Результаты:

    Summary:                                                 
    Count:        200                                      
    Total:        7.56 s                                   
    Slowest:      3.48 s                                   
    Fastest:      156.21 ms                                
    Average:      1.68 s                                   
    Requests/sec: 26.45
    
    Response time histogram:                                 
    156.213  [1]  |                                        
    488.977  [14] |∎∎∎∎∎∎∎                                 
    821.742  [8]  |∎∎∎∎                                    
    1154.506 [7]  |∎∎∎                                     
    1487.270 [8]  |∎∎∎∎                                    
    1820.035 [74] |∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎   
    2152.799 [81] |∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎∎
    2485.563 [4]  |∎∎                                      
    2818.328 [1]  |                                        
    3151.092 [0]  |                                        
    3483.856 [2]  |∎
    
    Latency distribution:                                    
    10 % in 653.42 ms                                      
    25 % in 1.72 s                                         
    50 % in 1.79 s                                         
    75 % in 2.01 s                                         
    90 % in 2.13 s                                         
    95 % in 2.14 s                                         
    99 % in 2.61 s
    
    Status code distribution:                                
    [OK]   200 responses     

Вывод: второй запрос эффективнее
