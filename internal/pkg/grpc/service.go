package grpc

import (
	"context"
	"github.com/golang/protobuf/ptypes/timestamp"
	"gitlab.ozon.dev/alkrev/homework-3.5_st/config"
	pb "gitlab.ozon.dev/alkrev/homework-3.5_st/pkg/api"
	"google.golang.org/protobuf/types/known/timestamppb"
)

type tserver struct {
	repo Repository
	cfg  config.Config
	pb.UnimplementedAirCraftFlightsServiceServer
}

func (ts tserver) SelectAirCraftFlights(ctx context.Context, req *pb.SelectAirCraftFlightsRequest) (*pb.SelectAirCraftFlightsResponse, error) {
	if acfs, err := ts.repo.ReadAirCraftFlights(ctx, req.Model); err != nil {
		return nil, err
	} else {
		var list []*pb.Flight
		for _, acf := range acfs {
			var ad *timestamp.Timestamp
			if acf.ActualDeparture != nil {
				ad = timestamppb.New(*acf.ActualDeparture)
			}
			var aa *timestamp.Timestamp
			if acf.ActualArrival != nil {
				aa = timestamppb.New(*acf.ActualArrival)
			}
			list = append(list, &pb.Flight{
				FlightId:           acf.FlightId,
				FlightsNo:          acf.FlightsNo,
				ScheduledDeparture: timestamppb.New(acf.ScheduledDeparture),
				ScheduledArrival:   timestamppb.New(acf.ScheduledArrival),
				DepartureAirport:   acf.DepartureAirport,
				ArrivalAirport:     acf.ArrivalAirport,
				AircraftCode:       acf.AircraftCode,
				ActualDeparture:    ad,
				ActualArrival:      aa,
			})
		}
		return &pb.SelectAirCraftFlightsResponse{
			List: list,
		}, nil
	}
}
func (ts tserver) SelectAirCraftFlights2(ctx context.Context, req *pb.SelectAirCraftFlightsRequest) (*pb.SelectAirCraftFlightsResponse, error) {
	if acfs, err := ts.repo.ReadAirCraftFlights2(ctx, req.Model); err != nil {
		return nil, err
	} else {
		var list []*pb.Flight
		for _, acf := range acfs {
			var ad *timestamp.Timestamp
			if acf.ActualDeparture != nil {
				ad = timestamppb.New(*acf.ActualDeparture)
			}
			var aa *timestamp.Timestamp
			if acf.ActualArrival != nil {
				aa = timestamppb.New(*acf.ActualArrival)
			}
			list = append(list, &pb.Flight{
				FlightId:           acf.FlightId,
				FlightsNo:          acf.FlightsNo,
				ScheduledDeparture: timestamppb.New(acf.ScheduledDeparture),
				ScheduledArrival:   timestamppb.New(acf.ScheduledArrival),
				DepartureAirport:   acf.DepartureAirport,
				ArrivalAirport:     acf.ArrivalAirport,
				AircraftCode:       acf.AircraftCode,
				ActualDeparture:    ad,
				ActualArrival:      aa,
			})
		}
		return &pb.SelectAirCraftFlightsResponse{
			List: list,
		}, nil
	}
}

func New(repo Repository, cfg config.Config) *tserver {
	return &tserver{repo: repo, cfg: cfg}
}
