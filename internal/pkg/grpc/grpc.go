package grpc

import (
	"context"
	"flag"
	"fmt"
	"gitlab.ozon.dev/alkrev/homework-3.5_st/config"
	pb "gitlab.ozon.dev/alkrev/homework-3.5_st/pkg/api"
	"golang.org/x/sync/errgroup"
	"google.golang.org/grpc"
	"net"
)

func Run(ctx context.Context, repo Repository, cfg config.Config) error {
	flag.Parse()
	apiServer := New(repo, cfg)
	lis, err := net.Listen("tcp", cfg.Grpc.Address)
	if err != nil {
		return fmt.Errorf("failed to listen: %v\n", err)
	}
	grpcServer := grpc.NewServer()
	pb.RegisterAirCraftFlightsServiceServer(grpcServer, apiServer)

	var group errgroup.Group

	group.Go(func() error {
		return grpcServer.Serve(lis)
	})

	group.Go(func() error {
		<-ctx.Done()
		grpcServer.GracefulStop()
		return ctx.Err()
	})

	return group.Wait()
}
