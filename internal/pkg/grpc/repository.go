package grpc

import (
	"context"
	"gitlab.ozon.dev/alkrev/homework-3.5_st/internal/models"
)

type Repository interface {
	ReadAirCraftFlights(ctx context.Context, model string) ([]models.Flights, error)
	ReadAirCraftFlights2(ctx context.Context, model string) ([]models.Flights, error)
}
