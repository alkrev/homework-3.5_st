package stress

import (
	"context"
	"fmt"
	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.ozon.dev/alkrev/homework-3.5_st/config"
	"gitlab.ozon.dev/alkrev/homework-3.5_st/internal/pkg/grpc"
	"gitlab.ozon.dev/alkrev/homework-3.5_st/internal/repository"
	"log"
	"os"
	"os/signal"
	"sync"
	"syscall"
)

func Run() {
	log.Println("starting")
	// Корректное завершение
	ctx := context.Background()
	ctx, cancel := context.WithCancel(ctx)
	errs := make(chan error)
	go func() {
		c := make(chan os.Signal, 50)
		signal.Notify(c, syscall.SIGHUP, syscall.SIGINT, syscall.SIGTERM, syscall.SIGQUIT)
		errs <- fmt.Errorf("%s", <-c)
		cancel()
	}()
	// Чтение настроек
	cfg, err := config.ReadConfig("./config/config.yaml")
	if err != nil {
		log.Fatal(err)
	}
	// Строка подключения
	connString := fmt.Sprintf("user=%s password=%s dbname=%s host=%s port=%s sslmode=disable", cfg.Database.User,
		cfg.Database.Password, cfg.Database.Dbname, cfg.Database.DBAddr, cfg.Database.DbPort)

	// Пул соединений с базой
	pool, _ := pgxpool.Connect(ctx, connString)
	if err := pool.Ping(ctx); err != nil {
		log.Fatal("error pinging db: ", err)
	}
	// Репозиторий
	repo := repository.New(pool)
	// Ожидаем завершения горутин
	var wg sync.WaitGroup

	// GRPC
	wg.Add(1)
	go func() {
		defer wg.Done()
		if err := grpc.Run(ctx, repo, *cfg); err != nil {
			log.Printf("grpc: %s\n", err)
		} else {
			log.Printf("grpc: stopped")
		}
		cancel()
	}()

	log.Println("started")
	log.Println(<-errs)
	wg.Wait()
	log.Println("exit")
}
