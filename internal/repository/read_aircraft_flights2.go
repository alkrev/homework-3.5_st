package repository

import (
	"context"
	"gitlab.ozon.dev/alkrev/homework-3.5_st/internal/models"
)

func (r *repository) ReadAirCraftFlights2(ctx context.Context, model string) ([]models.Flights, error) {
	const query = `
	select 
	  f.flight_id,
	  f.flight_no,
	  f.scheduled_departure,
	  f.scheduled_arrival,
	  f.departure_airport,
	  f.arrival_airport,
	  f.status,
	  f.aircraft_code,
	  f.actual_departure,
	  f.actual_arrival
    from aircrafts_data a, flights f, aircrafts ar 
    where a.aircraft_code = ar.aircraft_code and ar.aircraft_code = f.aircraft_code and ar.model = $1;
	`
	rows, err := r.pool.Query(ctx, query, model)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	var promoCodes []models.Flights
	for rows.Next() {
		var f models.Flights
		err := rows.Scan(
			&f.FlightId,
			&f.FlightsNo,
			&f.ScheduledDeparture,
			&f.ScheduledArrival,
			&f.DepartureAirport,
			&f.ArrivalAirport,
			&f.Status,
			&f.AircraftCode,
			&f.ActualDeparture,
			&f.ActualArrival,
		)
		if err != nil {
			return nil, err
		}
		promoCodes = append(promoCodes, f)
	}
	err = rows.Err()
	return promoCodes, err
}
