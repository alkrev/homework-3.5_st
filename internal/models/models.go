package models

import "time"

type Flights struct {
	FlightId           int64
	FlightsNo          string
	ScheduledDeparture time.Time
	ScheduledArrival   time.Time
	DepartureAirport   string
	ArrivalAirport     string
	Status             string
	AircraftCode       string
	ActualDeparture    *time.Time
	ActualArrival      *time.Time
}
