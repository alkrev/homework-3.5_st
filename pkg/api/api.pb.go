// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.28.0
// 	protoc        v3.6.1
// source: api/api.proto

package api

import (
	_ "github.com/golang/protobuf/ptypes/empty"
	timestamp "github.com/golang/protobuf/ptypes/timestamp"
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

type Flight struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	FlightId           int64                `protobuf:"varint,1,opt,name=flight_id,json=flightId,proto3" json:"flight_id,omitempty"`
	FlightsNo          string               `protobuf:"bytes,2,opt,name=flights_no,json=flightsNo,proto3" json:"flights_no,omitempty"`
	ScheduledDeparture *timestamp.Timestamp `protobuf:"bytes,3,opt,name=scheduled_departure,json=scheduledDeparture,proto3" json:"scheduled_departure,omitempty"`
	ScheduledArrival   *timestamp.Timestamp `protobuf:"bytes,4,opt,name=scheduled_arrival,json=scheduledArrival,proto3" json:"scheduled_arrival,omitempty"`
	DepartureAirport   string               `protobuf:"bytes,5,opt,name=departure_airport,json=departureAirport,proto3" json:"departure_airport,omitempty"`
	ArrivalAirport     string               `protobuf:"bytes,6,opt,name=arrival_airport,json=arrivalAirport,proto3" json:"arrival_airport,omitempty"`
	AircraftCode       string               `protobuf:"bytes,7,opt,name=aircraft_code,json=aircraftCode,proto3" json:"aircraft_code,omitempty"`
	ActualDeparture    *timestamp.Timestamp `protobuf:"bytes,8,opt,name=actual_departure,json=actualDeparture,proto3" json:"actual_departure,omitempty"`
	ActualArrival      *timestamp.Timestamp `protobuf:"bytes,9,opt,name=actual_arrival,json=actualArrival,proto3" json:"actual_arrival,omitempty"`
}

func (x *Flight) Reset() {
	*x = Flight{}
	if protoimpl.UnsafeEnabled {
		mi := &file_api_api_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *Flight) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*Flight) ProtoMessage() {}

func (x *Flight) ProtoReflect() protoreflect.Message {
	mi := &file_api_api_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use Flight.ProtoReflect.Descriptor instead.
func (*Flight) Descriptor() ([]byte, []int) {
	return file_api_api_proto_rawDescGZIP(), []int{0}
}

func (x *Flight) GetFlightId() int64 {
	if x != nil {
		return x.FlightId
	}
	return 0
}

func (x *Flight) GetFlightsNo() string {
	if x != nil {
		return x.FlightsNo
	}
	return ""
}

func (x *Flight) GetScheduledDeparture() *timestamp.Timestamp {
	if x != nil {
		return x.ScheduledDeparture
	}
	return nil
}

func (x *Flight) GetScheduledArrival() *timestamp.Timestamp {
	if x != nil {
		return x.ScheduledArrival
	}
	return nil
}

func (x *Flight) GetDepartureAirport() string {
	if x != nil {
		return x.DepartureAirport
	}
	return ""
}

func (x *Flight) GetArrivalAirport() string {
	if x != nil {
		return x.ArrivalAirport
	}
	return ""
}

func (x *Flight) GetAircraftCode() string {
	if x != nil {
		return x.AircraftCode
	}
	return ""
}

func (x *Flight) GetActualDeparture() *timestamp.Timestamp {
	if x != nil {
		return x.ActualDeparture
	}
	return nil
}

func (x *Flight) GetActualArrival() *timestamp.Timestamp {
	if x != nil {
		return x.ActualArrival
	}
	return nil
}

type SelectAirCraftFlightsRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Model string `protobuf:"bytes,1,opt,name=model,proto3" json:"model,omitempty"`
}

func (x *SelectAirCraftFlightsRequest) Reset() {
	*x = SelectAirCraftFlightsRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_api_api_proto_msgTypes[1]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *SelectAirCraftFlightsRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*SelectAirCraftFlightsRequest) ProtoMessage() {}

func (x *SelectAirCraftFlightsRequest) ProtoReflect() protoreflect.Message {
	mi := &file_api_api_proto_msgTypes[1]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use SelectAirCraftFlightsRequest.ProtoReflect.Descriptor instead.
func (*SelectAirCraftFlightsRequest) Descriptor() ([]byte, []int) {
	return file_api_api_proto_rawDescGZIP(), []int{1}
}

func (x *SelectAirCraftFlightsRequest) GetModel() string {
	if x != nil {
		return x.Model
	}
	return ""
}

type SelectAirCraftFlightsResponse struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	List []*Flight `protobuf:"bytes,1,rep,name=list,proto3" json:"list,omitempty"`
}

func (x *SelectAirCraftFlightsResponse) Reset() {
	*x = SelectAirCraftFlightsResponse{}
	if protoimpl.UnsafeEnabled {
		mi := &file_api_api_proto_msgTypes[2]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *SelectAirCraftFlightsResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*SelectAirCraftFlightsResponse) ProtoMessage() {}

func (x *SelectAirCraftFlightsResponse) ProtoReflect() protoreflect.Message {
	mi := &file_api_api_proto_msgTypes[2]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use SelectAirCraftFlightsResponse.ProtoReflect.Descriptor instead.
func (*SelectAirCraftFlightsResponse) Descriptor() ([]byte, []int) {
	return file_api_api_proto_rawDescGZIP(), []int{2}
}

func (x *SelectAirCraftFlightsResponse) GetList() []*Flight {
	if x != nil {
		return x.List
	}
	return nil
}

var File_api_api_proto protoreflect.FileDescriptor

var file_api_api_proto_rawDesc = []byte{
	0x0a, 0x0d, 0x61, 0x70, 0x69, 0x2f, 0x61, 0x70, 0x69, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x12,
	0x03, 0x61, 0x70, 0x69, 0x1a, 0x1b, 0x67, 0x6f, 0x6f, 0x67, 0x6c, 0x65, 0x2f, 0x70, 0x72, 0x6f,
	0x74, 0x6f, 0x62, 0x75, 0x66, 0x2f, 0x65, 0x6d, 0x70, 0x74, 0x79, 0x2e, 0x70, 0x72, 0x6f, 0x74,
	0x6f, 0x1a, 0x1f, 0x67, 0x6f, 0x6f, 0x67, 0x6c, 0x65, 0x2f, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x62,
	0x75, 0x66, 0x2f, 0x74, 0x69, 0x6d, 0x65, 0x73, 0x74, 0x61, 0x6d, 0x70, 0x2e, 0x70, 0x72, 0x6f,
	0x74, 0x6f, 0x22, 0xdf, 0x03, 0x0a, 0x06, 0x46, 0x6c, 0x69, 0x67, 0x68, 0x74, 0x12, 0x1b, 0x0a,
	0x09, 0x66, 0x6c, 0x69, 0x67, 0x68, 0x74, 0x5f, 0x69, 0x64, 0x18, 0x01, 0x20, 0x01, 0x28, 0x03,
	0x52, 0x08, 0x66, 0x6c, 0x69, 0x67, 0x68, 0x74, 0x49, 0x64, 0x12, 0x1d, 0x0a, 0x0a, 0x66, 0x6c,
	0x69, 0x67, 0x68, 0x74, 0x73, 0x5f, 0x6e, 0x6f, 0x18, 0x02, 0x20, 0x01, 0x28, 0x09, 0x52, 0x09,
	0x66, 0x6c, 0x69, 0x67, 0x68, 0x74, 0x73, 0x4e, 0x6f, 0x12, 0x4b, 0x0a, 0x13, 0x73, 0x63, 0x68,
	0x65, 0x64, 0x75, 0x6c, 0x65, 0x64, 0x5f, 0x64, 0x65, 0x70, 0x61, 0x72, 0x74, 0x75, 0x72, 0x65,
	0x18, 0x03, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x1a, 0x2e, 0x67, 0x6f, 0x6f, 0x67, 0x6c, 0x65, 0x2e,
	0x70, 0x72, 0x6f, 0x74, 0x6f, 0x62, 0x75, 0x66, 0x2e, 0x54, 0x69, 0x6d, 0x65, 0x73, 0x74, 0x61,
	0x6d, 0x70, 0x52, 0x12, 0x73, 0x63, 0x68, 0x65, 0x64, 0x75, 0x6c, 0x65, 0x64, 0x44, 0x65, 0x70,
	0x61, 0x72, 0x74, 0x75, 0x72, 0x65, 0x12, 0x47, 0x0a, 0x11, 0x73, 0x63, 0x68, 0x65, 0x64, 0x75,
	0x6c, 0x65, 0x64, 0x5f, 0x61, 0x72, 0x72, 0x69, 0x76, 0x61, 0x6c, 0x18, 0x04, 0x20, 0x01, 0x28,
	0x0b, 0x32, 0x1a, 0x2e, 0x67, 0x6f, 0x6f, 0x67, 0x6c, 0x65, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f,
	0x62, 0x75, 0x66, 0x2e, 0x54, 0x69, 0x6d, 0x65, 0x73, 0x74, 0x61, 0x6d, 0x70, 0x52, 0x10, 0x73,
	0x63, 0x68, 0x65, 0x64, 0x75, 0x6c, 0x65, 0x64, 0x41, 0x72, 0x72, 0x69, 0x76, 0x61, 0x6c, 0x12,
	0x2b, 0x0a, 0x11, 0x64, 0x65, 0x70, 0x61, 0x72, 0x74, 0x75, 0x72, 0x65, 0x5f, 0x61, 0x69, 0x72,
	0x70, 0x6f, 0x72, 0x74, 0x18, 0x05, 0x20, 0x01, 0x28, 0x09, 0x52, 0x10, 0x64, 0x65, 0x70, 0x61,
	0x72, 0x74, 0x75, 0x72, 0x65, 0x41, 0x69, 0x72, 0x70, 0x6f, 0x72, 0x74, 0x12, 0x27, 0x0a, 0x0f,
	0x61, 0x72, 0x72, 0x69, 0x76, 0x61, 0x6c, 0x5f, 0x61, 0x69, 0x72, 0x70, 0x6f, 0x72, 0x74, 0x18,
	0x06, 0x20, 0x01, 0x28, 0x09, 0x52, 0x0e, 0x61, 0x72, 0x72, 0x69, 0x76, 0x61, 0x6c, 0x41, 0x69,
	0x72, 0x70, 0x6f, 0x72, 0x74, 0x12, 0x23, 0x0a, 0x0d, 0x61, 0x69, 0x72, 0x63, 0x72, 0x61, 0x66,
	0x74, 0x5f, 0x63, 0x6f, 0x64, 0x65, 0x18, 0x07, 0x20, 0x01, 0x28, 0x09, 0x52, 0x0c, 0x61, 0x69,
	0x72, 0x63, 0x72, 0x61, 0x66, 0x74, 0x43, 0x6f, 0x64, 0x65, 0x12, 0x45, 0x0a, 0x10, 0x61, 0x63,
	0x74, 0x75, 0x61, 0x6c, 0x5f, 0x64, 0x65, 0x70, 0x61, 0x72, 0x74, 0x75, 0x72, 0x65, 0x18, 0x08,
	0x20, 0x01, 0x28, 0x0b, 0x32, 0x1a, 0x2e, 0x67, 0x6f, 0x6f, 0x67, 0x6c, 0x65, 0x2e, 0x70, 0x72,
	0x6f, 0x74, 0x6f, 0x62, 0x75, 0x66, 0x2e, 0x54, 0x69, 0x6d, 0x65, 0x73, 0x74, 0x61, 0x6d, 0x70,
	0x52, 0x0f, 0x61, 0x63, 0x74, 0x75, 0x61, 0x6c, 0x44, 0x65, 0x70, 0x61, 0x72, 0x74, 0x75, 0x72,
	0x65, 0x12, 0x41, 0x0a, 0x0e, 0x61, 0x63, 0x74, 0x75, 0x61, 0x6c, 0x5f, 0x61, 0x72, 0x72, 0x69,
	0x76, 0x61, 0x6c, 0x18, 0x09, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x1a, 0x2e, 0x67, 0x6f, 0x6f, 0x67,
	0x6c, 0x65, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x62, 0x75, 0x66, 0x2e, 0x54, 0x69, 0x6d, 0x65,
	0x73, 0x74, 0x61, 0x6d, 0x70, 0x52, 0x0d, 0x61, 0x63, 0x74, 0x75, 0x61, 0x6c, 0x41, 0x72, 0x72,
	0x69, 0x76, 0x61, 0x6c, 0x22, 0x34, 0x0a, 0x1c, 0x53, 0x65, 0x6c, 0x65, 0x63, 0x74, 0x41, 0x69,
	0x72, 0x43, 0x72, 0x61, 0x66, 0x74, 0x46, 0x6c, 0x69, 0x67, 0x68, 0x74, 0x73, 0x52, 0x65, 0x71,
	0x75, 0x65, 0x73, 0x74, 0x12, 0x14, 0x0a, 0x05, 0x6d, 0x6f, 0x64, 0x65, 0x6c, 0x18, 0x01, 0x20,
	0x01, 0x28, 0x09, 0x52, 0x05, 0x6d, 0x6f, 0x64, 0x65, 0x6c, 0x22, 0x40, 0x0a, 0x1d, 0x53, 0x65,
	0x6c, 0x65, 0x63, 0x74, 0x41, 0x69, 0x72, 0x43, 0x72, 0x61, 0x66, 0x74, 0x46, 0x6c, 0x69, 0x67,
	0x68, 0x74, 0x73, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12, 0x1f, 0x0a, 0x04, 0x6c,
	0x69, 0x73, 0x74, 0x18, 0x01, 0x20, 0x03, 0x28, 0x0b, 0x32, 0x0b, 0x2e, 0x61, 0x70, 0x69, 0x2e,
	0x46, 0x6c, 0x69, 0x67, 0x68, 0x74, 0x52, 0x04, 0x6c, 0x69, 0x73, 0x74, 0x32, 0xdd, 0x01, 0x0a,
	0x16, 0x41, 0x69, 0x72, 0x43, 0x72, 0x61, 0x66, 0x74, 0x46, 0x6c, 0x69, 0x67, 0x68, 0x74, 0x73,
	0x53, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x12, 0x60, 0x0a, 0x15, 0x53, 0x65, 0x6c, 0x65, 0x63,
	0x74, 0x41, 0x69, 0x72, 0x43, 0x72, 0x61, 0x66, 0x74, 0x46, 0x6c, 0x69, 0x67, 0x68, 0x74, 0x73,
	0x12, 0x21, 0x2e, 0x61, 0x70, 0x69, 0x2e, 0x53, 0x65, 0x6c, 0x65, 0x63, 0x74, 0x41, 0x69, 0x72,
	0x43, 0x72, 0x61, 0x66, 0x74, 0x46, 0x6c, 0x69, 0x67, 0x68, 0x74, 0x73, 0x52, 0x65, 0x71, 0x75,
	0x65, 0x73, 0x74, 0x1a, 0x22, 0x2e, 0x61, 0x70, 0x69, 0x2e, 0x53, 0x65, 0x6c, 0x65, 0x63, 0x74,
	0x41, 0x69, 0x72, 0x43, 0x72, 0x61, 0x66, 0x74, 0x46, 0x6c, 0x69, 0x67, 0x68, 0x74, 0x73, 0x52,
	0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x22, 0x00, 0x12, 0x61, 0x0a, 0x16, 0x53, 0x65, 0x6c,
	0x65, 0x63, 0x74, 0x41, 0x69, 0x72, 0x43, 0x72, 0x61, 0x66, 0x74, 0x46, 0x6c, 0x69, 0x67, 0x68,
	0x74, 0x73, 0x32, 0x12, 0x21, 0x2e, 0x61, 0x70, 0x69, 0x2e, 0x53, 0x65, 0x6c, 0x65, 0x63, 0x74,
	0x41, 0x69, 0x72, 0x43, 0x72, 0x61, 0x66, 0x74, 0x46, 0x6c, 0x69, 0x67, 0x68, 0x74, 0x73, 0x52,
	0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x1a, 0x22, 0x2e, 0x61, 0x70, 0x69, 0x2e, 0x53, 0x65, 0x6c,
	0x65, 0x63, 0x74, 0x41, 0x69, 0x72, 0x43, 0x72, 0x61, 0x66, 0x74, 0x46, 0x6c, 0x69, 0x67, 0x68,
	0x74, 0x73, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x22, 0x00, 0x42, 0x06, 0x5a, 0x04,
	0x2f, 0x61, 0x70, 0x69, 0x62, 0x06, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x33,
}

var (
	file_api_api_proto_rawDescOnce sync.Once
	file_api_api_proto_rawDescData = file_api_api_proto_rawDesc
)

func file_api_api_proto_rawDescGZIP() []byte {
	file_api_api_proto_rawDescOnce.Do(func() {
		file_api_api_proto_rawDescData = protoimpl.X.CompressGZIP(file_api_api_proto_rawDescData)
	})
	return file_api_api_proto_rawDescData
}

var file_api_api_proto_msgTypes = make([]protoimpl.MessageInfo, 3)
var file_api_api_proto_goTypes = []interface{}{
	(*Flight)(nil),                        // 0: api.Flight
	(*SelectAirCraftFlightsRequest)(nil),  // 1: api.SelectAirCraftFlightsRequest
	(*SelectAirCraftFlightsResponse)(nil), // 2: api.SelectAirCraftFlightsResponse
	(*timestamp.Timestamp)(nil),           // 3: google.protobuf.Timestamp
}
var file_api_api_proto_depIdxs = []int32{
	3, // 0: api.Flight.scheduled_departure:type_name -> google.protobuf.Timestamp
	3, // 1: api.Flight.scheduled_arrival:type_name -> google.protobuf.Timestamp
	3, // 2: api.Flight.actual_departure:type_name -> google.protobuf.Timestamp
	3, // 3: api.Flight.actual_arrival:type_name -> google.protobuf.Timestamp
	0, // 4: api.SelectAirCraftFlightsResponse.list:type_name -> api.Flight
	1, // 5: api.AirCraftFlightsService.SelectAirCraftFlights:input_type -> api.SelectAirCraftFlightsRequest
	1, // 6: api.AirCraftFlightsService.SelectAirCraftFlights2:input_type -> api.SelectAirCraftFlightsRequest
	2, // 7: api.AirCraftFlightsService.SelectAirCraftFlights:output_type -> api.SelectAirCraftFlightsResponse
	2, // 8: api.AirCraftFlightsService.SelectAirCraftFlights2:output_type -> api.SelectAirCraftFlightsResponse
	7, // [7:9] is the sub-list for method output_type
	5, // [5:7] is the sub-list for method input_type
	5, // [5:5] is the sub-list for extension type_name
	5, // [5:5] is the sub-list for extension extendee
	0, // [0:5] is the sub-list for field type_name
}

func init() { file_api_api_proto_init() }
func file_api_api_proto_init() {
	if File_api_api_proto != nil {
		return
	}
	if !protoimpl.UnsafeEnabled {
		file_api_api_proto_msgTypes[0].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*Flight); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_api_api_proto_msgTypes[1].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*SelectAirCraftFlightsRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_api_api_proto_msgTypes[2].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*SelectAirCraftFlightsResponse); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_api_api_proto_rawDesc,
			NumEnums:      0,
			NumMessages:   3,
			NumExtensions: 0,
			NumServices:   1,
		},
		GoTypes:           file_api_api_proto_goTypes,
		DependencyIndexes: file_api_api_proto_depIdxs,
		MessageInfos:      file_api_api_proto_msgTypes,
	}.Build()
	File_api_api_proto = out.File
	file_api_api_proto_rawDesc = nil
	file_api_api_proto_goTypes = nil
	file_api_api_proto_depIdxs = nil
}
